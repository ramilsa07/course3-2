DROP DATABASE IF EXISTS bookstore;
CREATE DATABASE bookstore;
USE bookstore;

CREATE TABLE author (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(50) NOT NULL,
  lastname VARCHAR(50) NOT NULL,
  patronymic VARCHAR(50) DEFAULT NULL,
  birthdate DATE NOT NULL,
  PRIMARY KEY (id),
  KEY firstname (firstname),
  KEY lastname (lastname),
  KEY patronymic (patronymic),
  KEY birthdate (birthdate)
);

CREATE TABLE author_address (
  id INT(11) NOT NULL AUTO_INCREMENT,
  authorid INT(11) NOT NULL,
  email VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY email (email),
  FOREIGN KEY (`authorid`) REFERENCES author (id) ON DELETE CASCADE
) ;

CREATE TABLE book (
  id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(50) NOT NULL,
  year INT(11) NOT NULL,
  pages INT(11) NOT NULL,
  PRIMARY KEY (id),
  KEY year (year),
  KEY pages (pages)
) ;

CREATE TABLE author_book (
  id INT(11) NOT NULL AUTO_INCREMENT,
  authorid INT(11) NOT NULL,
  bookid INT(11) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY author_book (authorid,bookid),
  KEY bookid (bookid),
  FOREIGN KEY (bookid) REFERENCES book (id) ON DELETE CASCADE,
  FOREIGN KEY (authorid) REFERENCES author (id) ON DELETE CASCADE
) ;

#1
insert into book values(null, "Folk songs", 1901, 123);
insert into book values(null, "Evgeny Onegin", 1930, 395);
insert into book values(null, "Comics", 2015, 38);
insert into book values(null, "Sherlock Holmes", 1982, 477);
insert into book values(null, "Graph theory", 2018, 93);
insert into book values(null, "Evgeny Onegin", 1930, 395);
insert into book values(null, "Evgeny Odincev", 1999, 9);
insert into book values(null, "Alexander Onegin", 2011, 1000);
#select * from book;

#2
#select * from book;
#SELECT * FROM book WHERE (id < 4) ;
#select * from book where id in (5);
#select * from book where title in ("Evgeny Onegin");
#select * from book where title like ("Evgeny%");
#select * from book where title like ("%Onegin");
#select * from book where (year > 1998 AND year< 2018);
#select * from book where (pages > 100 and pages < 500);
#select * from book where (pages > 100 and pages < 500) and (year < 1998 AND year > 1901);

#3
#UPDATE book SET pages = 1010;
#update book set pages = 3 where pages > 400;
#update book set pages = 3 where (pages > 400) and (year < 2000);
#update book set pages = 3 where (pages > 400) and (title like "Alexander%");
#SELECT * FROM book;

#4
#DELETE FROM book WHERE pages > 400; 
#delete from book where title like "evg%";
#delete from book where id > 2 and id < 6;
#delete from book where title in ("Evgeny Onegin");
#delete from book where title in ("Evgeny Onegin") or title like "evg%";
#DELETE FROM book; 
#SELECT * FROM book;

#6
#ALTER TABLE book ADD COLUMN publishing_house VARCHAR(50);
#ALTER TABLE book ADD COLUMN binding ENUM('WITHOUT_BINDING', 'SOFT', 'SOLID');

#7
#update book set binding = "WITHOUT_BINDING";

#8
#update book set publishing_house = "Russian culture" where id > 1 and id < 5;

#9
#ALTER TABLE book CHANGE title bookname VARCHAR(50);

#SELECT * FROM book;

#10
#RENAME TABLE book TO newbook;
#SELECT * FROM newbook;

#11
#DROP TABLE newbook;
#DROP TABLE IF EXISTS newbook ;
