package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;
import ru.omsu.imit.mybatis.dao.LotDAO;

import java.util.List;

public class LotDAOImpl extends BaseDAOImpl implements LotDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotDAOImpl.class);

    @Override
    public Lot insert(Lot lot) {
        LOGGER.debug("DAO Insert Lot {}", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getLotMapper(sqlSession).insert(lot);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Lot {}, {}", lot, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return lot;
    }

    @Override
    public Lot update(Lot lot) {
        LOGGER.debug("DAO update lot {} ", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getLotMapper(sqlSession).update(lot);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update lot {} {} ", lot, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return lot;
    }

    @Override
    public void updateCurPrice(final Lot lot, final int curPrice) {
        LOGGER.debug("DAO update lot {} curPrice {} ", lot, curPrice);
        if (curPrice <= lot.getCurPrice()) {
            LOGGER.error("new curPrice {} cannot be less than or equal to the curPrice {}", curPrice, lot.getCurPrice());
            throw new IllegalArgumentException("new curPrice cannot be less than or equal to the curPrice");
        }
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }

        try (SqlSession sqlSession = getSession()) {
            try {
                getLotMapper(sqlSession).updateCurPrice(curPrice, lot);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update curPrice {} {} ", curPrice, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public List<Lot> getAll() {
        LOGGER.debug("DAO get all lots");
        try (SqlSession sqlSession = getSession()) {
            return getLotMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all lots {}", ex);
            throw ex;
        }
    }

    @Override
    public Lot getById(int id) {
        LOGGER.debug("DAO Get Lot By Id {}", id);
        if (id < 0) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getLotMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Lot {}", ex);
            throw ex;
        }
    }

    @Override
    public void delete(Lot lot) {
        LOGGER.debug("DAO delete lot {} ", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getLotMapper(sqlSession).delete(lot);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete school {} {}", lot, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Lots {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getLotMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Lots {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public List<Lot> getByCategory(Category category) {
        LOGGER.debug("DAO get Lot by Category {}", category);
        if (!categoryIsCorrect(category)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getLotMapper(sqlSession).getByCategory(category);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Lot by Category {}, {}", category, ex);
            throw ex;
        }
    }
}
