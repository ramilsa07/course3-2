package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.List;

public interface LotMapper {
    @Insert("INSERT INTO lot (name, minPrice, curPrice) VALUES (#{lot.name}, #{lot.minPrice}, #{lot.curPrice})")
    @Options(useGeneratedKeys = true, keyProperty = "lot.id")
    Integer insert(@Param("lot") Lot lot);

    @Update("UPDATE lot SET name = #{lot.name}, minPrice = #{lot.minPrice}, curPrice = #{lot.curPrice} WHERE id = #{lot.id}")
    void update(@Param("lot") Lot lot);

    @Update("UPDATE lot SET curPrice = #{curPrice} WHERE id = #{lot.id}")
    void updateCurPrice(@Param("curPrice") final int curPrice, @Param("lot") final Lot lot);

    @Select("SELECT * FROM lot WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "bets", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.BetMapper.getLotBetList", fetchType = FetchType.LAZY))})
    Lot getById(int id);

    @Select("SELECT * FROM lot WHERE category_id = #{category.id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "bets", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.BetMapper.getLotBetList", fetchType = FetchType.LAZY))})
    List<Lot> getByCategory(@Param("category") Category category);

    @Select("SELECT * FROM lot")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "bets", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.BetMapper.getLotBetList", fetchType = FetchType.LAZY))})
    List<Lot> getAll();

    @Delete("DELETE FROM lot")
    void deleteAll();

    @Delete("DELETE FROM lot WHERE id = #{lot.id} ")
    int delete(@Param("lot") Lot lot);
}