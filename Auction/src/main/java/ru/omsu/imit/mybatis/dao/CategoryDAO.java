package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.model.Lot;
import ru.omsu.imit.model.Category;

import java.util.List;

public interface CategoryDAO {

    // вставляет Category в базу данных.
    Category insert(Category category);

    // получает Category по Lot
    Category getByLot(Lot lot);

    // получает Category по его ID
    Category getById(int id);

    // получает список всех Category вместе с Lot и Bet
    List<Category> getAllLazy();

    // изменяет Category в бд
    void update(Category category);

    // удаляет Category
    void delete(Category category);

    // удаляет все Category
    void deleteAll();

    // переводит Lot в Category. Если Lot не принадлежал никакой Category, добавляет его в Category
    Category moveLotToCategory(Category category, Lot lot);
}
