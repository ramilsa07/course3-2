package ru.omsu.imit.mybatis.dao;

public interface CommonDAO {
    // удаляет все записи из всех таблиц
    void clear();
}
