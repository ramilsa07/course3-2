package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;

import java.util.List;

public interface BuyerMapper {

    @Insert("INSERT INTO buyer (firstName, lastName) VALUES (#{buyer.firstName}, #{buyer.lastName})")
    @Options(useGeneratedKeys = true, keyProperty = "buyer.id")
    Integer insert(@Param("buyer") final Buyer buyer);

    @Insert({"<script>",
            "INSERT INTO buyer (firstName, lastName) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "( #{item.firstName}, #{item.lastName})",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true, keyProperty = "list.id")
    void batchInsert(@Param("list") List<Buyer> buyers);

    @Update("UPDATE buyer SET firstName = #{buyer.firstName}, lastName = #{buyer.lastName} WHERE id = #{buyer.id}")
    void update(@Param("buyer") final Buyer buyer);

    @Select("SELECT id, firstName, lastName FROM buyer WHERE id = #{id}")
    Buyer getById(int id);

    @Select("SELECT * FROM buyer")
    List<Buyer> getAll();

    @Select({"<script>",
            "SELECT id, firstName, lastName FROM buyer",
            "<where>",
            "<if test='firstName != null'> firstName LIKE #{firstName}",
            "</if>",

            "<if test='lastName != null'> AND lastName LIKE #{lastName}",
            "</if>",
            "</where>",
            "</script>"})
    List<Buyer> getAllWithParams(@Param("firstName") final String firstName, @Param("lastName") final String lastName);

    @Delete("DELETE FROM buyer WHERE id = #{buyer.id}")
    int delete(@Param("buyer") Buyer buyer);

    @Delete("DELETE FROM buyer")
    void deleteAll();
}
