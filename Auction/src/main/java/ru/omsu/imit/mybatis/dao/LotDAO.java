package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.List;

public interface LotDAO {

    // вставляет Lot в бд
    Lot insert(final Lot lot);

    // изменяет Lot в бд
    Lot update(final Lot lot);

    // изменяет текущую цену
    void updateCurPrice(final Lot lot, final int curPrice);

    // получает список всех Lot
    List<Lot> getAll();

    // получает Lot по ID
    Lot getById(final int id);

    // удаляет Lot
    void delete(final Lot lot);

    // удаляет все Lot
    void deleteAll();

    // возвращает лист лотов, принадлежащих Category
    List<Lot> getByCategory(final Category category);
}
