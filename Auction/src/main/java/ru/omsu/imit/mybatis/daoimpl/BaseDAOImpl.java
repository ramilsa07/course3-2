package ru.omsu.imit.mybatis.daoimpl;

import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;
import ru.omsu.imit.mybatis.mappers.CategoryMapper;
import ru.omsu.imit.mybatis.mappers.LotMapper;
import ru.omsu.imit.mybatis.mappers.BetMapper;
import ru.omsu.imit.mybatis.mappers.BuyerMapper;
import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected BetMapper getBetMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(BetMapper.class);
    }

    protected BuyerMapper getBuyerMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(BuyerMapper.class);
    }

    protected CategoryMapper getCategoryMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CategoryMapper.class);
    }

    protected LotMapper getLotMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(LotMapper.class);
    }

    protected boolean betIsCorrect(final Bet bet) {
        return bet.getId() >= 0 && bet.getPrice() >= 0;
    }

    protected boolean buyerIsCorrect(final Buyer buyer) {
        return buyer.getId() >= 0
                && !buyer.getFirstName().equals("")
                && !buyer.getLastName().equals("");
    }

    protected boolean categoryIsCorrect(final Category category) {
        return category.getId() >= 0
                && !category.getName().equals("");
    }

    protected boolean lotIsCorrect(final Lot lot) {
        return lot.getId() >= 0
                && !lot.getName().equals("")
                && lot.getCurPrice() >= 0
                && lot.getMinPrice() >= 0
                && lot.getCurPrice() >= lot.getMinPrice();
    }
}
