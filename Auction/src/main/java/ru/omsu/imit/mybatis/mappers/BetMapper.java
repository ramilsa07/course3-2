package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.List;


public interface BetMapper {

    @Insert("INSERT INTO bet (price, buyer_id) VALUES (#{bet.price}, #{buyer.id})")
    @Options(useGeneratedKeys = true, keyProperty = "bet.id")
    Integer insert(@Param("bet") Bet bet, @Param("buyer") Buyer buyer);

    @Insert("INSERT INTO bet (price, buyer_id, lot_id) VALUES (#{bet.price}, #{buyer.id}, #{lot.id})")
    @Options(useGeneratedKeys = true, keyProperty = "bet.id")
    Integer insertWithLot(@Param("bet") Bet bet, @Param("buyer") Buyer buyer, @Param("lot") Lot lot);

    @Select("SELECT * FROM bet WHERE id = #{id}")
    Bet getById(int id);

    @Delete("DELETE FROM bet WHERE id = #{bet.id}")
    int delete(@Param("bet") Bet bet);

    @Delete("DELETE FROM bet WHERE lot_id = #{lot.id}")
    int deleteBetsOnLot(@Param("lot") Lot lot);

    @Delete("DELETE FROM bet WHERE buyer_id = #{buyer.id}")
    int deleteBetsOnBuyer(@Param("buyer") Buyer buyer);

    @Select("SELECT * FROM bet WHERE lot_id = #{lot.id}")
    List<Bet> getLotBetList(@Param("lot") Lot lot);

    @Select("SELECT * FROM bet WHERE buyer_id = #{buyer.id}")
    List<Bet> getBuyerBetList(@Param("buyer") Buyer buyer);
}
