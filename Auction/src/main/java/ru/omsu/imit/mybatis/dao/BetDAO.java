package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Lot;
import ru.omsu.imit.model.Buyer;

import java.util.List;

public interface BetDAO {

    // вставляет Bet в бд, привязывает ее к Buyer.
    Bet insert(Bet bet, Buyer buyer);

    // вставляет Bet в бд, привязывает ее к Buyer и Lot.
    Bet insertWithLot(Bet bet, Buyer buyer, Lot lot);

    // получает Bet по его ID. Если такого ID нет, метод возвращает null
    Bet getById(int id);

    // получает все Bet по определенному Lot
    List<Bet> getLotBetList(Lot lot);

    // получает все Bet поставленные Buyer
    List<Bet> getBuyerBetList(Buyer buyer);

    // удаляет ставки, поставленные Buyer
    void deleteBetsOnBuyer(Buyer buyer);

    // удаляет ставки на определенный Lot
    void deleteBetsOnLot(Lot lot);

    // удаляет Bet из бд
    void delete(Bet bet);
}
