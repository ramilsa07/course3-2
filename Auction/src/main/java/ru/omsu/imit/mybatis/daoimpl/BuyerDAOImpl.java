package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.mybatis.dao.BuyerDAO;

import java.util.List;

public class BuyerDAOImpl extends BaseDAOImpl implements BuyerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuyerDAOImpl.class);

    @Override
    public Buyer insert(Buyer buyer) {
        LOGGER.debug("DAO insert buyer {}", buyer);
        if (!buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).insert(buyer);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert buyer {}, {}", buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return buyer;
    }

    @Override
    public Buyer update(Buyer buyer) {
        LOGGER.debug("DAO update buyer {} ", buyer);
        if (!buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).update(buyer);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update trainee {} {}", buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return buyer;
    }

    @Override
    public Buyer getById(int id) {
        LOGGER.debug("DAO get buyer by id {}", id);
        if (id < 0) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getBuyerMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get buyer {}", ex);
            throw ex;
        }
    }

    @Override
    public List<Buyer> getAll() {
        LOGGER.debug("DAO get all buyer");
        try (SqlSession sqlSession = getSession()) {
            return getBuyerMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all trainee {}", ex);
            throw ex;
        }
    }

    @Override
    public List<Buyer> getAllWithParams(final String firstName, final String lastName) {
        LOGGER.debug("DAO get all buyer with params firstName {}, lastName {}", firstName, lastName);
        if (firstName.equals("") || lastName.equals("")) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getBuyerMapper(sqlSession).getAllWithParams(firstName, lastName);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all buyer with params {}", ex);
            throw ex;
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO delete all buyers");
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all buyers {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(Buyer buyer) {
        LOGGER.debug("DAO delete buyer {} ", buyer);
        if (!buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).delete(buyer);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete buyer {} {}", buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void batchInsert(List<Buyer> buyerList) {
        LOGGER.debug("DAO batch insert buyers {} ", buyerList);
        for (Buyer buyer : buyerList) {
            if (!buyerIsCorrect(buyer)) {
                LOGGER.error("incorrect arguments passed to the method Bet insert");
                throw new IllegalArgumentException("incorrect arguments passed to the method");
            }
        }

        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).batchInsert(buyerList);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't batch insert buyers {} {} ", buyerList, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
