package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.model.Lot;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.mybatis.dao.CategoryDAO;

import java.util.List;

public class CategoryDAOImpl extends BaseDAOImpl implements CategoryDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDAOImpl.class);

    @Override
    public Category insert(Category category) {
        LOGGER.debug("DAO Insert Category {}", category);
        if (!categoryIsCorrect(category)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).insert(category);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Category {}, {}", category, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return category;
    }

    @Override
    public void delete(Category category) {
        LOGGER.debug("DAO delete category {} ", category);
        if (!categoryIsCorrect(category)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).delete(category);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete school {} {}", category, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Category getByLot(Lot lot) {
        LOGGER.debug("DAO get Category by Lot {}", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getByLot(lot);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Category by Lot {}, {}", lot, ex);
            throw ex;
        }
    }


    @Override
    public Category getById(int id) {
        LOGGER.debug("DAO Get Category By Id {}", id);
        if (id < 0) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Category by id {}, {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<Category> getAllLazy() {
        LOGGER.debug("DAO get all category lazy");
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getAllLazy();
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all category {}", ex);
            throw ex;
        }
    }

    @Override
    public void update(Category category) {
        LOGGER.debug("DAO update category {} ", category);
        if (!categoryIsCorrect(category)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).update(category);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update category {} {} ", category, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Categories {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Categories {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Category moveLotToCategory(Category category, Lot lot) {
        LOGGER.debug("DAO move lot {} to category {} ", lot, category);
        if (!categoryIsCorrect(category) || !lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).moveLotToCategory(lot, category);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't move lot {} to category{} {}", lot, category, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return category;
    }
}
