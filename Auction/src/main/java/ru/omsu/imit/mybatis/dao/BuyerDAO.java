package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;

import java.util.List;

public interface BuyerDAO {

    // вставляет Buyer в бд
    Buyer insert(final Buyer buyer);

    // изменяет Buyer в бд
    Buyer update(final Buyer buyer);

    // получает Buyer по идентификатору
    Buyer getById(final int id);

    // получает всех Buyer
    List<Buyer> getAll();

    // получает список всех Buyer при условиях
    // если firstName не равен null - только имеющих это имя
    // если lastName не равен null - только имеющих эту фамилию
    List<Buyer> getAllWithParams(final String firstName, final String lastName);

    // удаляет все Buyer
    void deleteAll();

    // удаляет Buyer
    void delete(Buyer buyer);

    // вставляет список из Trainee в базу данных
    void batchInsert(List<Buyer> buyerList);
}
