package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.List;

public interface CategoryMapper {

    @Insert("INSERT INTO category (name) VALUES (#{category.name})")
    @Options(useGeneratedKeys = true, keyProperty = "category.id")
    Integer insert(@Param("category") Category category);

    @Update("UPDATE category SET name = #{category.name} WHERE id = #{category.id}")
    void update(@Param("category") Category category);

    @Select("SELECT * FROM category WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "lots", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.LotMapper.getByCategory", fetchType = FetchType.LAZY))
    })
    Category getById(int id);

    @Select("SELECT * FROM category WHERE id IN (SELECT category_id FROM lot WHERE id = #{lot.id})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "lots", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.LotMapper.getByCategory", fetchType = FetchType.LAZY))
    })
    Category getByLot(@Param("lot") Lot lot);

    @Select("SELECT * FROM category")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "lots", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.LotMapper.getByCategory", fetchType = FetchType.LAZY))
    })
    List<Category> getAllLazy();

    @Update("UPDATE lot SET category_id = #{category.id} WHERE id = #{lot.id}")
    void moveLotToCategory(@Param("lot") Lot lot, @Param("category") Category category);

    @Delete("DELETE FROM category")
    void deleteAll();

    @Delete("DELETE FROM category WHERE id = #{category.id}")
    int delete(@Param("category") Category category);
}
