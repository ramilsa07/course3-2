package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.model.Lot;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.mybatis.dao.BetDAO;

import java.util.List;

public class BetDAOImpl extends BaseDAOImpl implements BetDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BetDAOImpl.class);

    @Override
    public Bet insert(final Bet bet, final Buyer buyer) {
        LOGGER.debug("DAO Insert Bet {} from a Buyer {}", bet, buyer);
        if (!betIsCorrect(bet) || !buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBetMapper(sqlSession).insert(bet, buyer);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Bet {} from a Buyer {}, {}", bet, buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return bet;
    }

    @Override
    public Bet insertWithLot(final Bet bet, final Buyer buyer, final Lot lot) {
        LOGGER.debug("DAO Insert Bet {} from a Buyer {} for Lot {}", bet, buyer, lot);
        if (!betIsCorrect(bet) || !buyerIsCorrect(buyer) || !lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBetMapper(sqlSession).insertWithLot(bet, buyer, lot);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Bet {} from a Buyer {} for Lot {}, {}", bet, buyer, lot, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return bet;
    }

    @Override
    public Bet getById(int id) {
        LOGGER.debug("BetDAO get bet by id {}", id);
        if (id < 0) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getBetMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Bet {}", ex);
            throw ex;
        }
    }

    @Override
    public List<Bet> getLotBetList(Lot lot) {
        LOGGER.debug("DAO get bet by lot {}", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getBetMapper(sqlSession).getLotBetList(lot);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get bet by lot {}, {}", lot, ex);
            throw ex;
        }
    }

    @Override
    public List<Bet> getBuyerBetList(Buyer buyer) {
        LOGGER.debug("DAO get bet by buyer {}", buyer);
        if (!buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            return getBetMapper(sqlSession).getBuyerBetList(buyer);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get bet by buyer {}, {}", buyer, ex);
            throw ex;
        }
    }

    @Override
    public void deleteBetsOnLot(Lot lot) {
        LOGGER.debug("DAO delete bets on lot {} ", lot);
        if (!lotIsCorrect(lot)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBetMapper(sqlSession).deleteBetsOnLot(lot);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete bets on lot {} {}", lot, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(Bet bet) {
        LOGGER.debug("DAO delete bet {} ", bet);
        if (!betIsCorrect(bet)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBetMapper(sqlSession).delete(bet);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete bet {} {}", bet, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteBetsOnBuyer(Buyer buyer) {
        LOGGER.debug("DAO delete bets on buyer {} ", buyer);
        if (!buyerIsCorrect(buyer)) {
            LOGGER.error("incorrect arguments passed to the method Bet insert");
            throw new IllegalArgumentException("incorrect arguments passed to the method");
        }
        try (SqlSession sqlSession = getSession()) {
            try {
                getBetMapper(sqlSession).deleteBetsOnBuyer(buyer);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete bets on buyer {} {}", buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
