package ru.omsu.imit.model;

import java.util.Objects;

public class Bet {

    private int id;
    private int price;

    public Bet() {
    }

    public Bet(int id, int price) {
        this.id = id;
        this.price = price;
    }

    public Bet(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Bet)) return false;
        final Bet bet = (Bet) o;
        return getId() == bet.getId() && getPrice() == bet.getPrice();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPrice());
    }

    @Override
    public String toString() {
        return "Bet{" +
                "id=" + id +
                ", price=" + price +
                '}';
    }
}
