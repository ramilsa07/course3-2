package ru.omsu.imit.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lot {

    private int id;
    private String name;
    private int minPrice;
    private int curPrice;
    private List<Bet> bets;

    public Lot() {
    }

    public Lot(int id, String name, int minPrice, List<Bet> bets) {
        this.id = id;
        this.name = name;
        this.minPrice = minPrice;
        curPrice = minPrice;
        this.bets = bets;
    }

    public Lot(int id, String name, int minPrice) {
        this.id = id;
        this.name = name;
        this.minPrice = minPrice;
        curPrice = minPrice;
        this.bets = new ArrayList<>();
    }

    public Lot(String name, int minPrice) {
        this.name = name;
        this.minPrice = minPrice;
        curPrice = minPrice;
        this.bets = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(final int minPrice) {
        this.minPrice = minPrice;
    }

    public int getCurPrice() {
        return curPrice;
    }

    public void setCurPrice(final int curPrice) {
        this.curPrice = curPrice;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public void addBet(final Bet bet) {
        bets.add(bet);
    }

    public void removeBet(final Bet bet) {
        bets.remove(bet);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Lot)) return false;
        final Lot lot = (Lot) o;
        return getId() == lot.getId() && getMinPrice() == lot.getMinPrice() && getCurPrice() == lot.getCurPrice() && Objects.equals(getName(), lot.getName()) && Objects.equals(getBets(), lot.getBets());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getMinPrice(), getCurPrice(), getBets());
    }

    @Override
    public String toString() {
        return "Lot{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", minPrice=" + minPrice +
                ", curPrice=" + curPrice +
                ", bets=" + bets +
                '}';
    }
}
