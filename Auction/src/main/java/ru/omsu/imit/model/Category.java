package ru.omsu.imit.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Category {

    private int id;
    private String name;
    private List<Lot> lots;

    public Category(int id, String name, List<Lot> lots) {
        this.id = id;
        this.name = name;
        this.lots = lots;
    }

    public Category(int id, String name) {
        this(id, name, new ArrayList<Lot>());
    }

    public Category(String name, List<Lot> lots) {
        this(0, name, lots);
    }

    public Category(String name) {
        this(0, name, new ArrayList<>());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addLot(final Lot lot) {
        lots.add(lot);
    }

    public void removeLot(final Lot lot) {
        lots.remove(lot);
    }

    public List<Lot> getLots() {
        return lots;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        final Category category = (Category) o;
        return getId() == category.getId() && Objects.equals(getName(), category.getName()) && Objects.equals(getLots(), category.getLots());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLots());
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lots=" + lots +
                '}';
    }
}
