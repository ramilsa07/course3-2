package ru.omsu.imit.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Buyer {

    private int id;
    private String firstName;
    private String lastName;
    private List<Bet> bets;

    public Buyer() {
    }

    public Buyer(int id, String firstName, String lastName, List<Bet> bets) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.bets = bets;
    }

    public Buyer(int id, String firstName, String lastName) {
        this(id, firstName, lastName, null);
    }

    public Buyer(String firstName, String lastName) {
        this(0, firstName, lastName, null);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public void addBet(final Bet bet) {
        if (bets == null) {
            bets = new ArrayList<>();
        }
        bets.add(bet);
    }

    // если сделал ставку - пути назад нет. Но на всякий случай оставим
    public void removeBet(final Bet bet) {
        bets.remove(bet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buyer)) return false;
        Buyer buyer = (Buyer) o;
        return getId() == buyer.getId() && Objects.equals(getFirstName(), buyer.getFirstName()) && Objects.equals(getLastName(), buyer.getLastName()) && Objects.equals(getBets(), buyer.getBets());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getBets());
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "id=" + id +
                ", name='" + firstName + '\'' +
                ", surname='" + lastName + '\'' +
                ", bets=" + bets +
                '}';
    }
}
