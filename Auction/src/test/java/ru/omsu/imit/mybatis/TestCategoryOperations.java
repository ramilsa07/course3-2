package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestCategoryOperations extends TestBase {

    @Test
    public void testInsertCategory() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);
    }

    @Test
    public void testBadInsertCategory() {
        assertThrows(IllegalArgumentException.class, () -> {
            Category category = insertCategory("");
        });
    }

    @Test
    public void testGetByLot() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);

        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);
        category.addLot(lot);
        categoryDAO.moveLotToCategory(category, lot);

        assertEquals(category, categoryDAO.getByLot(lot));
    }

    @Test
    public void testUpdateCategory() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);

        category.setName("classic book");
        categoryDAO.update(category);
        assertEquals(category, categoryDAO.getById(category.getId()));
    }

    @Test
    public void testDeleteCategory() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);

        categoryDAO.delete(category);
        assertNull(categoryDAO.getById(category.getId()));
    }

    @Test
    public void getAllLazy() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);

        List<Lot> allLotFromCategory = new ArrayList<>();

        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);
        categoryDAO.moveLotToCategory(category, lot);
        allLotFromCategory.add(lot);

        Lot lot1 = insertLot("Master and Margarita", 100);
        Lot lot1FromDB = lotDAO.getById(lot1.getId());
        assertEquals(lot1, lot1FromDB);
        categoryDAO.moveLotToCategory(category, lot1);
        allLotFromCategory.add(lot1);

        Lot lot2 = insertLot("Lord of the Rings", 110);
        Lot lot2FromDB = lotDAO.getById(lot2.getId());
        assertEquals(lot2, lot2FromDB);
        categoryDAO.moveLotToCategory(category, lot2);
        allLotFromCategory.add(lot2);

        assertEquals(allLotFromCategory, lotDAO.getByCategory(category));

        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Buyer buyerPetrov = insertBuyer("Петр", "Петров");
        Buyer buyerPetrovFromDB = buyerDAO.getById(buyerPetrov.getId());
        assertEquals(buyerPetrov, buyerPetrovFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov, lot);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);
        lot.addBet(betIvanov);

        Bet bet1Ivanov = insertBet(1000, buyerIvanov, lot1);
        Bet bet1IvanovFromDB = betDAO.getById(bet1Ivanov.getId());
        assertEquals(bet1Ivanov, bet1IvanovFromDB);
        lot1.addBet(bet1Ivanov);

        Bet betPetrov = insertBet(150, buyerPetrov, lot);
        Bet betPetrovFromDB = betDAO.getById(betPetrov.getId());
        assertEquals(betPetrov, betPetrovFromDB);
        lot.addBet(betPetrov);

        Bet betPetrov1 = insertBet(1500, buyerPetrov, lot2);
        Bet betPetrov1FromDB = betDAO.getById(betPetrov1.getId());
        assertEquals(betPetrov1, betPetrov1FromDB);
        lot2.addBet(betPetrov1);

        category.addLot(lot);
        category.addLot(lot1);
        category.addLot(lot2);

        Category category1 = insertCategory("toys");
        Category category1FromDB = categoryDAO.getById(category1.getId());
        assertEquals(category1, category1FromDB);

        List<Category> allCategory = new ArrayList<>();
        allCategory.add(category);
        allCategory.add(category1);

        assertEquals(allCategory, categoryDAO.getAllLazy());
    }
}
