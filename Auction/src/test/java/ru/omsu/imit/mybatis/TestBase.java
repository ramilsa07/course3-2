package ru.omsu.imit.mybatis;

import org.junit.Before;
import org.junit.BeforeClass;
import ru.omsu.imit.model.*;
import ru.omsu.imit.mybatis.dao.*;
import ru.omsu.imit.mybatis.daoimpl.*;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestBase {

    protected BetDAO betDAO = new BetDAOImpl();
    protected BuyerDAO buyerDAO = new BuyerDAOImpl();
    protected CategoryDAO categoryDAO = new CategoryDAOImpl();
    protected LotDAO lotDAO = new LotDAOImpl();

    private static boolean setUpIsDone = false;

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = MyBatisUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() {
        buyerDAO.deleteAll();
        categoryDAO.deleteAll();
        lotDAO.deleteAll();
    }

    protected Buyer insertBuyer(final String firstName, final String lastName) {
        Buyer buyer = new Buyer(firstName, lastName);
        buyerDAO.insert(buyer);
        assertNotEquals(0, buyer.getId());
        return buyer;
    }

    protected Bet insertBet(final int price, final Buyer buyer) {
        Bet bet = new Bet(price);
        betDAO.insert(bet, buyer);
        assertNotEquals(0, bet.getId());
        return bet;
    }

    protected Bet insertBet(final int price, final Buyer buyer, final Lot lot) {
        Bet bet = new Bet(price);
        betDAO.insertWithLot(bet, buyer, lot);
        assertNotEquals(0, bet.getId());
        return bet;
    }

    protected Lot insertLot(final String name, final int minPrice) {
        Lot lot = new Lot(name, minPrice);
        lotDAO.insert(lot);
        assertNotEquals(0, lot.getId());
        return lot;
    }

    protected Category insertCategory(final String name) {
        Category category = new Category(name);
        categoryDAO.insert(category);
        assertNotEquals(0, category.getId());
        return category;
    }
}
