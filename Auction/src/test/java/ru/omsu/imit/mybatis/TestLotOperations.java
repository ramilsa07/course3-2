package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Category;
import ru.omsu.imit.model.Lot;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestLotOperations extends TestBase {

    @Test
    public void testInsertLot() {
        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);
    }

    @Test
    public void testInsertLotWithNullName() {
        assertThrows(RuntimeException.class, () -> {
            Lot lot = insertLot(null, 100);
        });
    }

    @Test
    public void testInsertLotWithNegativePrice() {
        assertThrows(IllegalArgumentException.class, () -> {
            Lot lot = insertLot("Mu-mu", -90);
        });
    }

    @Test
    public void testUpdateLot() {
        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        lot.setName("Not_mu-mu");
        lot.setMinPrice(200);
        lotDAO.update(lot);
        lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);
    }

    @Test
    public void testUpdateLotWithNullName() {
        assertThrows(IllegalArgumentException.class, () -> {
            Lot lot = insertLot("Mu-mu", 90);
            Lot lotFromDB = lotDAO.getById(lot.getId());
            assertEquals(lot, lotFromDB);

            lot.setName("");
            lot.setMinPrice(200);
            lotDAO.update(lot);
        });
    }

    @Test
    public void testUpdateCurPrice() {
        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov, lot);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);

        lotDAO.updateCurPrice(lot, 100);
        assertEquals(100, lotDAO.getById(lot.getId()).getCurPrice());
    }

    @Test
    public void testBadUpdateCurPrice() {
        assertThrows(IllegalArgumentException.class, () -> {
            Lot lot = insertLot("Mu-mu", 90);
            Lot lotFromDB = lotDAO.getById(lot.getId());
            assertEquals(lot, lotFromDB);

            Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
            Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
            assertEquals(buyerIvanov, buyerIvanovFromDB);

            Bet betIvanov = insertBet(100, buyerIvanov, lot);
            Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
            assertEquals(betIvanov, betIvanovFromDB);

            lotDAO.updateCurPrice(lot, -100);
        });
    }

    @Test
    public void testBadUpdateCurPrice2() {
        assertThrows(IllegalArgumentException.class, () -> {
            Lot lot = insertLot("Mu-mu", 90);
            Lot lotFromDB = lotDAO.getById(lot.getId());
            assertEquals(lot, lotFromDB);

            Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
            Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
            assertEquals(buyerIvanov, buyerIvanovFromDB);

            Bet betIvanov = insertBet(100, buyerIvanov, lot);
            Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
            assertEquals(betIvanov, betIvanovFromDB);

            lotDAO.updateCurPrice(lot, 90);
        });
    }

    @Test
    public void testGetAllLots() {

        List<Lot> allLots = new ArrayList<>();

        Lot lot = insertLot("Mu-mu", 90);
        allLots.add(lot);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Lot lot1 = insertLot("Master and Margarita", 100);
        allLots.add(lot1);
        Lot lot1FromDB = lotDAO.getById(lot1.getId());
        assertEquals(lot1, lot1FromDB);

        Lot lot2 = insertLot("Lord of the Rings", 110);
        allLots.add(lot2);
        Lot lot2FromDB = lotDAO.getById(lot2.getId());
        assertEquals(lot2, lot2FromDB);

        List<Lot> allLotsFromDB = lotDAO.getAll();
        assertEquals(allLots, allLotsFromDB);
    }

    @Test
    public void testDeleteLot() {
        List<Lot> allLots = new ArrayList<>();

        Lot lot = insertLot("Mu-mu", 90);
        allLots.add(lot);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Lot lot1 = insertLot("Master and Margarita", 100);
        allLots.add(lot1);
        Lot lot1FromDB = lotDAO.getById(lot1.getId());
        assertEquals(lot1, lot1FromDB);

        Lot lot2 = insertLot("Lord of the Rings", 110);
        allLots.add(lot2);
        Lot lot2FromDB = lotDAO.getById(lot2.getId());
        assertEquals(lot2, lot2FromDB);

        lotDAO.delete(lot);
        allLots.remove(lot);
        List<Lot> allLotsFromDB = lotDAO.getAll();
        assertEquals(allLots, allLotsFromDB);
    }

    @Test
    public void testDeleteAllLots() {

        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Lot lot1 = insertLot("Master and Margarita", 100);
        Lot lot1FromDB = lotDAO.getById(lot1.getId());
        assertEquals(lot1, lot1FromDB);

        Lot lot2 = insertLot("Lord of the Rings", 110);
        Lot lot2FromDB = lotDAO.getById(lot2.getId());
        assertEquals(lot2, lot2FromDB);

        lotDAO.deleteAll();
        assertEquals(new ArrayList<>(), lotDAO.getAll());
    }

    @Test
    public void testGetByCategory() {
        Category category = insertCategory("book");
        Category categoryFromDB = categoryDAO.getById(category.getId());
        assertEquals(category, categoryFromDB);

        List<Lot> allLotFromCategory = new ArrayList<>();

        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);
        categoryDAO.moveLotToCategory(category, lot);
        allLotFromCategory.add(lot);

        Lot lot1 = insertLot("Master and Margarita", 100);
        Lot lot1FromDB = lotDAO.getById(lot1.getId());
        assertEquals(lot1, lot1FromDB);
        categoryDAO.moveLotToCategory(category, lot1);
        allLotFromCategory.add(lot1);

        Lot lot2 = insertLot("Lord of the Rings", 110);
        Lot lot2FromDB = lotDAO.getById(lot2.getId());
        assertEquals(lot2, lot2FromDB);
        categoryDAO.moveLotToCategory(category, lot2);
        allLotFromCategory.add(lot2);

        assertEquals(allLotFromCategory, lotDAO.getByCategory(category));
    }
}
