package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.model.Bet;
import ru.omsu.imit.model.Buyer;
import ru.omsu.imit.model.Lot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestBetOperations extends TestBase {

    @Test
    public void testInsertBet() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);
    }

    @Test
    public void testInsertBetWithNullBuyer() {
        assertThrows(RuntimeException.class, () -> {
            Bet betIvanov = insertBet(100, null);
        });
    }

    @Test
    public void testInsertBetWithLot() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov, lot);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);

        Bet betIvanov2 = insertBet(110, buyerIvanov, lot);
        Bet betIvanov2FromDB = betDAO.getById(betIvanov2.getId());
        assertEquals(betIvanov2, betIvanov2FromDB);

        List<Bet> bets = new ArrayList<>();
        bets.add(betIvanov);
        bets.add(betIvanov2);
        List<Bet> betsFromDB = betDAO.getLotBetList(lot);
        betsFromDB.sort(Comparator.comparingInt(Bet::getId));
        assertEquals(bets, betsFromDB);
    }

    @Test
    public void testInsertEqualBet() {
        assertThrows(RuntimeException.class, () -> {
            Lot lot = insertLot("Mu-mu", 90);
            Lot lotFromDB = lotDAO.getById(lot.getId());
            assertEquals(lot, lotFromDB);

            Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
            Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
            assertEquals(buyerIvanov, buyerIvanovFromDB);

            Buyer buyerPetrov = insertBuyer("Петр", "Петров");
            Buyer buyerPetrovFromDB = buyerDAO.getById(buyerPetrov.getId());
            assertEquals(buyerPetrov, buyerPetrovFromDB);

            Bet betIvanov = insertBet(100, buyerIvanov, lot);
            Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
            assertEquals(betIvanov, betIvanovFromDB);

            Bet betPetrov = insertBet(100, buyerPetrov, lot);
        });
    }

    @Test
    public void testDeleteBet() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");

        Bet betIvanov = insertBet(100, buyerIvanov);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);

        betDAO.delete(betIvanov);
        assertNull(betDAO.getById(betIvanov.getId()));
    }

    @Test
    public void testDeleteBetsOnLot() {
        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Buyer buyerPetrov = insertBuyer("Петр", "Петров");
        Buyer buyerPetrovFromDB = buyerDAO.getById(buyerPetrov.getId());
        assertEquals(buyerPetrov, buyerPetrovFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov, lot);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);

        Bet betPetrov = insertBet(150, buyerPetrov, lot);
        Bet betPetrovFromDB = betDAO.getById(betPetrov.getId());
        assertEquals(betPetrov, betPetrovFromDB);

        List<Bet> allBetsOnLot = new ArrayList<>();
        allBetsOnLot.add(betIvanov);
        allBetsOnLot.add(betPetrov);

        List<Bet> allBetsOnLotFromDB = betDAO.getLotBetList(lot);
        assertEquals(allBetsOnLotFromDB, allBetsOnLot);

        betDAO.deleteBetsOnLot(lot);
        assertEquals(betDAO.getLotBetList(lot), new ArrayList<>());
    }

    @Test
    public void testDeleteBetsOnBuyer() {
        Lot lot = insertLot("Mu-mu", 90);
        Lot lotFromDB = lotDAO.getById(lot.getId());
        assertEquals(lot, lotFromDB);

        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);

        Buyer buyerPetrov = insertBuyer("Петр", "Петров");
        Buyer buyerPetrovFromDB = buyerDAO.getById(buyerPetrov.getId());
        assertEquals(buyerPetrov, buyerPetrovFromDB);

        Bet betIvanov = insertBet(100, buyerIvanov, lot);
        Bet betIvanovFromDB = betDAO.getById(betIvanov.getId());
        assertEquals(betIvanov, betIvanovFromDB);

        Bet betPetrov = insertBet(150, buyerPetrov, lot);
        Bet betPetrovFromDB = betDAO.getById(betPetrov.getId());
        assertEquals(betPetrov, betPetrovFromDB);

        Bet betIvanov2 = insertBet(200, buyerIvanov, lot);
        Bet betIvanov2FromDB = betDAO.getById(betIvanov2.getId());
        assertEquals(betIvanov2, betIvanov2FromDB);

        List<Bet> allBetsOnLot = new ArrayList<>();
        allBetsOnLot.add(betIvanov);
        allBetsOnLot.add(betPetrov);
        allBetsOnLot.add(betIvanov2);

        List<Bet> allBetsOnLotFromDB = betDAO.getLotBetList(lot);
        assertEquals(allBetsOnLotFromDB, allBetsOnLot);

        List<Bet> allIvanovBets = new ArrayList<>();
        allIvanovBets.add(betIvanov);
        allIvanovBets.add(betIvanov2);

        List<Bet> allIvanovBetsFromDB = betDAO.getBuyerBetList(buyerIvanov);
        assertEquals(allIvanovBets, allIvanovBetsFromDB);

        betDAO.deleteBetsOnBuyer(buyerIvanov);
        assertEquals(betDAO.getBuyerBetList(buyerIvanov), new ArrayList<>());
    }
}
