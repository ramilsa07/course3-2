package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.model.Buyer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestBuyerOperations extends TestBase {

    @Test
    public void testInsertBuyer() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);
    }

    @Test
    public void testInsertBuyerWithNullFirstName() {
        assertThrows(RuntimeException.class, () -> {
            Buyer buyerIvanov = new Buyer(null, "Ivanov");
            buyerDAO.insert(buyerIvanov);
        });
    }

    @Test
    public void testInsertTwoBuyer() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerPetrov = insertBuyer("Петр", "Петров");
        List<Buyer> buyers = new ArrayList<>();
        buyers.add(buyerIvanov);
        buyers.add(buyerPetrov);
        List<Buyer> buyersFromDB = buyerDAO.getAll();
        assertEquals(buyers, buyersFromDB);
    }

    @Test
    public void testUpdateBuyer() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);
        buyerIvanov.setFirstName("NotIvan");
        buyerIvanov.setLastName("NotIvanov");
        buyerDAO.update(buyerIvanov);
        buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);
    }

    @Test
    public void testUpdateBuyerSetNullLastName() {
        assertThrows(RuntimeException.class, () -> {
            Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
            Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
            assertEquals(buyerIvanov, buyerIvanovFromDB);
            buyerIvanov.setLastName(null);
            buyerDAO.update(buyerIvanov);
        });
    }

    @Test
    public void testIfCondition() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerPetrovPetr = insertBuyer("Петр", "Петров");
        Buyer buyerPetrovFedor = insertBuyer("Федор", "Петров");
        Buyer buyerSidorov = insertBuyer("Петр", "Сидоров");
        Buyer buyerSidorenko = insertBuyer("Иван", "Сидоренко");

        List<Buyer> buyersFull = new ArrayList<>();
        buyersFull.add(buyerIvanov);
        buyersFull.add(buyerPetrovPetr);
        buyersFull.add(buyerPetrovFedor);
        buyersFull.add(buyerSidorov);
        buyersFull.add(buyerSidorenko);

        List<Buyer> buyersIvan = new ArrayList<>();
        buyersIvan.add(buyerIvanov);
        buyersIvan.add(buyerSidorenko);

        List<Buyer> buyersSidor = new ArrayList<>();
        buyersSidor.add(buyerSidorov);
        buyersSidor.add(buyerSidorenko);

        List<Buyer> buyersFullFromDB = buyerDAO.getAll();
        buyersFullFromDB.sort(Comparator.comparingInt(Buyer::getId));
        assertEquals(buyersFull, buyersFullFromDB);

        List<Buyer> buyersIvanFromDB = buyerDAO.getAllWithParams("Иван", null);
        buyersIvanFromDB.sort(Comparator.comparingInt(Buyer::getId));
        assertEquals(buyersIvan, buyersIvanFromDB);

        List<Buyer> buyersSidorFromDB = buyerDAO.getAllWithParams(null, "Сидор%");
        buyersSidorFromDB.sort(Comparator.comparingInt(Buyer::getId));
        assertEquals(buyersSidor, buyersSidorFromDB);
    }

    @Test
    public void testBatchInsertBuyers() {
        Buyer buyerIvanov = new Buyer("Иван", "Иванов");
        Buyer buyerPetrovPetr = new Buyer("Петр", "Петров");
        Buyer buyerPetrovFedor = new Buyer("Федор", "Петров");
        Buyer buyerSidorov = new Buyer("Петр", "Сидоров");
        Buyer buyerSidorenko = new Buyer("Иван", "Сидоренко");

        List<Buyer> buyersFull = new ArrayList<>();
        buyersFull.add(buyerIvanov);
        buyersFull.add(buyerPetrovPetr);
        buyersFull.add(buyerPetrovFedor);
        buyersFull.add(buyerSidorov);
        buyersFull.add(buyerSidorenko);
        buyerDAO.batchInsert(buyersFull);
        List<Buyer> buyersFullFromDB = buyerDAO.getAll();
        buyersFullFromDB.sort(Comparator.comparingInt(Buyer::getId));
        assertEquals(buyersFull, buyersFullFromDB);
    }

    @Test
    public void testDeleteBuyer() {
        Buyer buyerIvanov = insertBuyer("Иван", "Иванов");
        Buyer buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertEquals(buyerIvanov, buyerIvanovFromDB);
        buyerDAO.delete(buyerIvanov);
        buyerIvanovFromDB = buyerDAO.getById(buyerIvanov.getId());
        assertNull(buyerIvanovFromDB);
    }
}
