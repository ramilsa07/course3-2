DROP DATABASE IF EXISTS auction;
CREATE DATABASE auction;
USE auction;

CREATE TABLE category
(
    id   INT         NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY ukey (name)
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

CREATE TABLE lot
(
    id          INT         NOT NULL AUTO_INCREMENT,
    name        VARCHAR(45) NOT NULL,
    minPrice    INT         NOT NULL,
    curPrice    INT         NOT NULL,
    category_id INT DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE SET NULL
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

CREATE TABLE buyer
(
    id        INT         NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(45) NOT NULL,
    lastName  VARCHAR(45) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;

CREATE TABLE bet
(
    id       INT NOT NULL AUTO_INCREMENT,
    price    INT NOT NULL,
    buyer_id INT NOT NULL,
    lot_id   INT DEFAULT NULL,
    PRIMARY KEY (id, buyer_id),
    UNIQUE KEY ukey (price, lot_id), # т.к. нельзя поставить одинаковый размер ставки на один лот
    FOREIGN KEY (buyer_id) REFERENCES buyer (id) ON DELETE CASCADE,
    FOREIGN KEY (lot_id) REFERENCES lot (id) ON DELETE SET NULL
) ENGINE = INNODB
  DEFAULT CHARSET = utf8;