package ru.omsu.imit.task1;

import java.sql.*;

public class _4_Delete {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore?useUnicode=yes&characterEncoding=UTF8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    private static void getBooksByColNames(Connection con) {
        String selectQuery = "select * from book";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    //  Удалить записи о книгах количество страниц которых больше P
    private static void deleteBook1(Connection con) {
        String deleteQuery = "DELETE FROM book WHERE pages > 400";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//  Удалить записи о книгах название которых начинается со слова W
    private static void deleteBook2(Connection con) {
        String deleteQuery = "delete from book where title like \"evg%\"";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

// id которых от id1 до id2
    private static void deleteBook3(Connection con) {
        String deleteQuery = "delete from book where id > 2 and id < 6";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

// с названием N
    private static void deleteBook4(Connection con) {
        String deleteQuery = "delete from book where id > 2 and id < 6;";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//  с названиями N1, N2 и N3
    private static void deleteBook5(Connection con) {
        String deleteQuery = "delete from book where id > 2 and id < 6;";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//  все книги
    private static void deleteBook6(Connection con) {
        String deleteQuery = "delete from book where id > 2 and id < 6;";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void main(String args[]) {

        if (!loadDriver()) {
            return;
        }

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {

            deleteBook1(con);
            System.out.println("Number of pages greater than P");
            getBooksByColNames(con);

            deleteBook2(con);
            System.out.println("The name begins with the word W");
            getBooksByColNames(con);

            deleteBook3(con);
            System.out.println("id whose id1 to id2");
            getBooksByColNames(con);

            deleteBook4(con);
            System.out.println("With name N");
            getBooksByColNames(con);

            deleteBook5(con);
            System.out.println("With name N, N1, N2");
            getBooksByColNames(con);

            deleteBook6(con);
            System.out.println("Delete all books");
            getBooksByColNames(con);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
