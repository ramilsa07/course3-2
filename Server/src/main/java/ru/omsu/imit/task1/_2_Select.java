package ru.omsu.imit.task1;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class _2_Select {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore?useUnicode=yes&characterEncoding=UTF8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    private static void getBooksByColNames(Connection con) {
        String selectQuery = "select * from book";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private static void get3Books(Connection con) {
        String selectQuery = "SELECT * FROM book WHERE (id < 4)";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private static void getBookById(Connection con) {
        String selectQuery = "select * from book where id in (5)";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private static void getRecordsWithTheSpecifiedName(Connection con) {
        String selectQuery = "select * from book where title in (\"Evgeny Onegin\")";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private static void AllEntriesWithNameStartingWithWord(Connection con) {
        String selectQuery = "select * from book where title like (\"Evgeny%\")";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private static void AllEntriesWithNameEndingWithWord(Connection con) {
        String selectQuery = "select * from book where title like (\"%Onegin\")";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void RecordsBooksPublishedOverTime(Connection con) {
        String selectQuery = "select * from book where (year > 1998 AND year< 2018)";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void BookEntriesWithPageSpacing(Connection con) {
        String selectQuery = "select * from book where (pages > 100 and pages < 500)";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void BookEntriesWithSpanOfPagesAndSpanOfYears(Connection con) {
        String selectQuery = "select * from book where (pages > 100 and pages < 500) and (year < 1998 AND year > 1901)";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*private static void getAuthorsByColNumbers(Connection con) {

        String selectQuery = "select * from author";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt(1);
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);
                String patronymic = rs.getString(4);
                Date birthDate = rs.getDate(5);
                Author author = new Author(id, firstName, lastName, patronymic, birthDate);
                System.out.println(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }
    private static void insertAuthor(Connection con) {
        String insertQuery = "insert into author values(null,'Ìèõàèë','Ôåäîðîâ',null,'1960-7-1')";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(insertQuery, Statement.RETURN_GENERATED_KEYS);
            if (result > 0) {
                System.out.println("Result = " + result);
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println("id = " + id);
                    rs.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void updateAuthor(Connection con) {
        String updateQuery = "update author set firstname ='Ãðèãîðèé' where lastname='Ôåäîðîâ'";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void deleteAuthor(Connection con) {
        String deleteQuery = "delete from author where lastname='Ôåäîðîâ'";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(deleteQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
*/
    public static void main(String args[]) {

        if (!loadDriver()) {
            return;
        }

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {

            System.out.println("Books by column names");
            getBooksByColNames(con);
            System.out.println();

            System.out.println("Get 3 books");
            get3Books(con);
            System.out.println();

            System.out.println("Get Book By Id");
            getBookById(con);
            System.out.println();

            System.out.println("Get records with the specified name");
            getRecordsWithTheSpecifiedName(con);
            System.out.println();

            System.out.println("All entries with a name starting with the word (\"Evgeny\")");
            AllEntriesWithNameStartingWithWord(con);
            System.out.println();

            System.out.println("All entries with a name ending with the word \"%Onegin\"");
            AllEntriesWithNameEndingWithWord(con);
            System.out.println();

            System.out.println("Records of books published over time");
            RecordsBooksPublishedOverTime(con);
            System.out.println();

            System.out.println("Book Entries with Page Spacing");
            BookEntriesWithPageSpacing(con);
            System.out.println();

            System.out.println("Book entries with a span of pages and a span of years");
            BookEntriesWithSpanOfPagesAndSpanOfYears(con);
            System.out.println();

            /*System.out.println("Authors by column numbers");
            getAuthorsByColNumbers(con);

            insertAuthor(con);

            System.out.println("Authors by column names");
            getAuthorsByColNames(con);

            updateAuthor(con);
            System.out.println("Authors after update");
            getAuthorsByColNames(con);

            deleteAuthor(con);
            System.out.println("Authors after delete");
            getAuthorsByColNames(con);*/

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
}
