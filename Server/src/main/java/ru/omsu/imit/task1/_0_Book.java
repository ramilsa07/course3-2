package ru.omsu.imit.task1;

public class _0_Book {
    private int id;
    private String title;
    private int year;
    private int pages;
    private String binding;
    private String publishing_house;


    public _0_Book(int id, String title, int year, int pages) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.pages = pages;
    }

    public _0_Book(int id, String title, int year, int pages, String binding, String publishing_house) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.pages = pages;
        this.binding = binding;
        this.publishing_house = publishing_house;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", pages=" + pages +
                ", binding='" + binding + '\'' +
                ", publishing_house='" + publishing_house + '\'' +
                '}';
    }
}
