package ru.omsu.imit.task1;

import java.sql.*;

public class _7_11_Tasks {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore?useUnicode=yes&characterEncoding=UTF8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    private static void getBooksByColNames(Connection con) {
        String selectQuery = "select * from book";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                String binding = rs.getString("binding");
                String publishing_house = rs.getString("publishing_house");
                _0_Book book = new _0_Book(id, title, year, pages, binding, publishing_house);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    // Установить всем книгам в таблице тип переплета “без переплета”
    private static void update1(Connection con) {
        String updateQuery = "update book set binding = \"WITHOUT_BINDING\"";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Установить книгам с id от ID1 до ID2 поле “издательство” в P
    private static void update2(Connection con) {
        String updateQuery = "update book set publishing_house = \"Russian culture\" where id > 1 and id < 5";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Изменить название столбца ‘title’  на ‘bookname’
    private static void change1(Connection con) {
        String changeQuery = "ALTER TABLE book CHANGE title bookname VARCHAR(50)";
        try (Statement stmt = con.createStatement()) {
            stmt.execute(changeQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void getBooksByChange1(Connection con) {
        String selectQuery = "select * from book";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String bookname = rs.getString("bookname");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                String binding = rs.getString("binding");
                String publishing_house = rs.getString("publishing_house");
                _0_Book book = new _0_Book(id, bookname, year, pages, binding, publishing_house);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    // Изменить название таблицы.
    private static void change2(Connection con) {
        String changeQuery = "RENAME TABLE book TO newbook";
        try (Statement stmt = con.createStatement()) {
            stmt.execute(changeQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void getBooksByChange2(Connection con) {
        String selectQuery = "select * from newbook";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String bookname = rs.getString("bookname");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                String binding = rs.getString("binding");
                String publishing_house = rs.getString("publishing_house");
                _0_Book book = new _0_Book(id, bookname, year, pages, binding, publishing_house);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    // Удалить таблицу.
    private static void deleteDataBase(Connection con) {
        String changeQuery = "DROP TABLE IF EXISTS book";
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate(changeQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        if (!loadDriver()) {
            return;
        }

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {

//            update1(con);
//            System.out.println("ex7");
//            getBooksByColNames(con);

//            update2(con);
//            System.out.println("ex8");
//            getBooksByColNames(con);

//            change1(con);
//            System.out.println("ex9");
//            getBooksByChange1(con);

//            change2(con);
//            System.out.println("ex10");
//            getBooksByChange2(con);

            deleteDataBase(con);
            System.out.println("ex11");
            getBooksByColNames(con);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
}
