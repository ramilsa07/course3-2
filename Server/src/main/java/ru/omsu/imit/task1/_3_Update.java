package ru.omsu.imit.task1;

import java.sql.*;

public class _3_Update {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore?useUnicode=yes&characterEncoding=UTF8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    private static void getBooksByColNames(Connection con) {
        String selectQuery = "select * from book";
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(selectQuery)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                _0_Book book = new _0_Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

// установить для всех книг количество страниц, равное P
    private static void AllBooksHaveNPages(Connection con) {
        String updateQuery = "UPDATE book SET pages = 1010";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

// установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2
    private static void SetNumberOfPages1(Connection con) {
        String updateQuery = "update book set pages = 3 where pages > 400";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

// установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2 и год выпуска раньше Y
    private static void SetNumberOfPages2(Connection con) {
        String updateQuery = "update book set pages = 3 where (pages > 400) and (year < 2000)";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2 и название начинается со слова W
    private static void SetNumberOfPages3(Connection con) {
        String updateQuery = "update book set pages = 3 where (pages > 400) and (title like \"Alexander%\")";
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(updateQuery);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {

        if (!loadDriver()) {
            return;
        }

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {

//            AllBooksHaveNPages(con);
//            System.out.println("All Books Have N Pages");
//            getBooksByColNames(con);
//            System.out.println();

//            SetNumberOfPages1(con);
//            System.out.println("Set the number of pages equal to P1 for all books whose number of pages is greater than P2");
//            getBooksByColNames(con);
//            System.out.println();

//            SetNumberOfPages2(con);
//            System.out.println("set the number of pages equal to P1 for all books whose number of pages is greater than P2 and the year of publication is earlier than Y");
//            getBooksByColNames(con);
//            System.out.println();

//            SetNumberOfPages3(con);
//            System.out.println("set the number of pages equal to P1 for all books whose number of pages is greater than P2 and the name begins with the word W");
//            getBooksByColNames(con);
//            System.out.println();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
}
