package ru.omsu.imit.task2.common;

public class Settings {
    public static final int REST_HTTP_PORT = 8888;
    public static final String SERVER_ADDRESS = "localhost";
}
