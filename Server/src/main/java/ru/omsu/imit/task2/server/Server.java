package ru.omsu.imit.task2.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.task2.common.Settings;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    public static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();

        try (ServerSocket serverSocket = new ServerSocket(Settings.REST_HTTP_PORT)) {
            LOGGER.info("Server started : port {}", Settings.REST_HTTP_PORT);
            while (true) {
                LOGGER.debug("Waiting accept from client");
                Socket socket = serverSocket.accept();
                executorService.execute(new ServerThread(socket));
            }
        } catch (IOException e) {
            LOGGER.error("Error with up server socket ", e);
        } catch (RuntimeException e) {
            LOGGER.error("Fatal error ", e);
        }finally {
            LOGGER.info("Server stopped");
        }
    }
}
