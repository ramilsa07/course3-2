package ru.omsu.imit.task2.client;

import ru.omsu.imit.task2.common.Settings;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(Settings.SERVER_ADDRESS);
        } catch (UnknownHostException e) {
            return;
        }
        try (Socket socket = new Socket(ipAddress, Settings.REST_HTTP_PORT);
             DataInputStream in = new DataInputStream(socket.getInputStream());
             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in))) {
            String line = null;
            line = in.readUTF();
            System.out.println(line);
            while (true) {
                line = keyboardReader.readLine();
                out.writeUTF(line);
                out.flush();
                line = in.readUTF();
                System.out.println(line);
                if("Guessed".equalsIgnoreCase(line)){
                    socket.close();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
