package ru.omsu.imit.task2.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;

public class ServerThread implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerThread.class);
    private Socket client;

    public ServerThread(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        int limit = 10000;
        int tryCount = 0;
        Random random = new Random(System.currentTimeMillis());
        int number = random.nextInt(limit);
        String clientAddress = client.getInetAddress().toString();
        try (DataInputStream inputStream = new DataInputStream(client.getInputStream());
             DataOutputStream outputStream = new DataOutputStream(client.getOutputStream())) {
            LOGGER.info("Server start connection with client " + clientAddress);
            LOGGER.debug(clientAddress + "'s number for guessing " + number);
            outputStream.writeUTF("Try to guess number");
            outputStream.flush();
            while (1 == 1) {
                String str = inputStream.readUTF();
                LOGGER.debug(clientAddress + " send " + str);
                if (str == null || str.length() == 0) {
                    continue;
                }
                int clientNumber = Integer.parseInt(str);
                LOGGER.debug(clientAddress + " number is " + clientNumber);
                if (clientNumber == number) {
                    outputStream.writeUTF("Guessed");
                    outputStream.flush();
                    LOGGER.info(clientAddress + " guessed");
                    break;
                }
                if (clientNumber > number) {
                    tryCount++;
                    outputStream.writeUTF("Less");
                    outputStream.flush();
                }
                if (clientNumber < number) {
                    tryCount++;
                    outputStream.writeUTF("More");
                    outputStream.flush();
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error while talking with client", e);
        } finally {
            try {
                LOGGER.info(clientAddress + " had " + tryCount + " attempt");
                client.close();
            } catch (IOException e) {
                LOGGER.error("Error while close client " + clientAddress);
            }
        }
    }
}

